# Trident

This is the Trident project. This project combines 'Chat' and 'S1' (Project S1) into one program. The program is a hybrid, it can be used for numerous things including Server/Client connection and command-to-system communications.

The program is written in Java (JavaFX for GUI).